<?php

namespace Tests\Unit\Repositories;

use Tests\TestCase;
use App\Models\Vehicle;
use App\Repositories\VehicleRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VehicleRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function tesGetAllByUser()
    {

        $data = $this->getAllData();


        $rentsAdded = array();
        $rentsAdded[] = factory(Rent::class)->create($data[0]);
        $rentsAdded[] = factory(Rent::class)->create($data[1]);
        $rentsAdded[] = factory(Rent::class)->create($data[2]);


        $all = $this->getAllByUserFilteredResponse("3", true);

        foreach($all as $single){

            $this->assertEquals($single['user_id'], "3");
        }
    }

     private function getAllByUserFilteredResponse($idUser){

        $vehicleRepository = \App::make(VehicleRepository::class);

        $all = app()->make(VehicleRepository::class)->getAllByUser($idUser)->toArray();
        foreach($all as &$single){

            unset($single['user']);
            unset($single['created_at']);
            unset($single['updated_at']);
            unset($single['id']);
        }
        return $all;
    }



    public function testCreate()
    {

        $data = $this->getAllData();

        $added = array();
        $added[] = factory(Vehicle::class)->create($data[0]);
         

        $all = $this->getFilteredResponse();
      
        $this->assertContains($data[0], $all);
    }

    public function testUpdate()
    {

        $data = $this->getAllData();

        $added  = factory(Vehicle::class)->create($data[0]);

        $data[0]['title'] = "Elcihev 1";
        $data[0]['image'] = $added->id.".jpg";


        app()->make(VehicleRepository::class)->update($data[0],$added->id);

        $all = $this->getFilteredResponse();
     
        $this->assertContains($data[0], $all);
    }

    /////


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAll()
    {
         $data = $this->getAllData();

         $ordersAdded = array();
         $ordersAdded[] = factory(Vehicle::class)->create($data[0]);
         $ordersAdded[] = factory(Vehicle::class)->create($data[1]);
         $ordersAdded[] = factory(Vehicle::class)->create($data[2]);

		 $all = $this->getFilteredResponse();
      
         $this->assertContains($data[0], $all);
         $this->assertContains($data[1], $all);
         $this->assertContains($data[2], $all);

    }




	private function getFilteredResponse(){

		$vehicleRepository = \App::make(VehicleRepository::class);

		$all = app()->make(VehicleRepository::class)->getAll()->toArray();
		foreach($all as &$single){

			unset($single['user']);
			unset($single['created_at']);
			unset($single['updated_at']);
			unset($single['id']);
		}
    	return $all;
    }


    private function getAllData(){

    	return [
            [
                "title" => "Vehicle 1",
                "description" => "Vehicle 1 description",
                "image" => "Vehicle 1 image",
                "user_id" => "1"

            ],
            [
                "title" => "Vehicle 2",
                "description" => "Vehicle 2 description",
                "image" => "Vehicle 2 image",
                "user_id" => "2"
            ],
            [
                "title" => "Vehicle 3",
                "description" => "Vehicle 3 description",
                "image" => "Vehicle 3 image",
                "user_id" => "3"
            ]
        ];
    }
}
