<?php

namespace Tests\Unit\Repositories;

use Tests\TestCase;
use App\Models\Rent;
use App\Repositories\RentRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;   
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RentRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetAllByVehicle(){

        $data = $this->getCreateData();


        $rentsAdded = array();
        $rentsAdded[] = factory(Rent::class)->create($data[0]);
        $rentsAdded[] = factory(Rent::class)->create($data[1]);
        $rentsAdded[] = factory(Rent::class)->create($data[2]);


        $all = $this->getAllByVehicleFilteredResponse("2", true);

        foreach($all as $single){

            $this->assertEquals($single['vehicle_id'], "2");
        }
    }

    private function getAllByVehicleFilteredResponse($vehicle, $id = false){

        $vehicleRepository = \App::make(RentRepository::class);

        $all = app()->make(RentRepository::class)->getAllByVehicle($vehicle)->toArray();
        foreach($all as &$single){

            unset($single['created_at']);
            unset($single['updated_at']);
            if(!$id)
            {
                unset($single['id']);
            }
        }
        return $all;
    }



    public function testAccept(){

        $data = $this->getCreateData();

        $rentAdded = factory(Rent::class)->create($data[1]);
         
        app()->make(RentRepository::class)->accept($rentAdded->id);

        $all = $this->getFilteredResponse(true);

        foreach($all as $single){
            if($single['id'] == $rentAdded->id)
            {
                $this->assertEquals($single['status'], "2");
            }
        }
      
         
    }
/////


    /**
     * Get All Test.
     *
     * @return void
     */
    public function testGetAll()
    {
         $data = $this->getCreateData();

         $rentsAdded = array();
         $rentsAdded[] = factory(Rent::class)->create($data[0]);
         $rentsAdded[] = factory(Rent::class)->create($data[1]);
         $rentsAdded[] = factory(Rent::class)->create($data[2]);

		 $all = $this->getFilteredResponse();
      
         $this->assertContains($data[0], $all);
         $this->assertContains($data[1], $all);
         $this->assertContains($data[2], $all);

    }
    /**
     * Create Test.
     *
     * @return void
     */
     public function testCreate()
    {
         $data = $this->getCreateData();

         $rentsAdded = array();
         $rentsAdded[] = factory(Rent::class)->create($data[0]);
         

		 $all = $this->getFilteredResponse();
      
         $this->assertContains($data[0], $all);
         

    }
     /**
     * Get Filtered response for create and get all test.
     *
     * @return void
     */
    private function getFilteredResponse($id = false){

        $vehicleRepository = \App::make(RentRepository::class);

        $all = app()->make(RentRepository::class)->getAll()->toArray();
        foreach($all as &$single){

            unset($single['created_at']);
            unset($single['updated_at']);
            if(!$id)
            {
                unset($single['id']);
            }
        }
        return $all;
    }



     /**
     * Get static data for create and get all test.
     *
     * @return void
     */
    private function getCreateData(){

        return [
            [
                 "start" => "2017-07-16 00:00:00",
                "end" => "2017-07-18 23:59:59",
                "renter" => "Renter 1",
                "vehicle_id" => "1",
                "price" => "2000",
                "payment_status" => "1",
                "status" => "2"
                

            ],
            [
                "start" => "2017-07-15 19:39:33",
                "end" => "2017-07-17 19:39:33",
                "renter" => "Renter 2",
                "vehicle_id" => "2",
                "price" => "15555",
                "payment_status" => "0",
                "status" => "1"

            ],
            [
                "start" => "2017-06-12 19:39:33",
                "end" => "2017-06-13 19:39:33",
                "renter" => "Renter 3",
                "vehicle_id" => "1",
                "price" => "1234",
                "payment_status" => "1",
                "status" => "0"

            ]
        ];
    }








    public function testFindDatesByVehicle()
    {
         $data = $this->getCreateData();

         $rentsAdded = array();
         $rentsAdded[] = factory(Rent::class)->create($data[0]);
         

         $all = $this->getFindDatesByVehicleFilteredResponse();

         $data = $this->getShowData();


         $this->assertEquals($data[0], $all["2017-07-16"]);
         
         $this->assertEquals($data[1], $all["2017-07-17"]);

         $this->assertEquals($data[2], $all["2017-07-18"]);



    }
    private function getFindDatesByVehicleFilteredResponse(){

        $vehicleRepository = \App::make(RentRepository::class);

        $all = app()->make(RentRepository::class)->findDatesByVehicle(1);

        foreach($all as &$single){
            if(isset($single[1]) && is_array($single[1]))
            {
                unset($single[1]['created_at']);
                unset($single[1]['updated_at']);
                unset($single[1]['id']);
                unset($single[1]['vehicle']);
            }
        
        }
        return $all;
    }

 
     /**
     * Get actual data for show test.
     *
     * @return void
     */
    private function getShowData(){

        return [
            [
                  "2017-07-16",
                  array(
                        "start" => "2017-07-16 00:00:00",
                        "end" => "2017-07-18 23:59:59",
                        "renter" => "Renter 1",
                        "vehicle_id" => "1",
                        "price" => "2000",
                        "payment_status" => "1",
                        "status" => "2"
                        )

            ],
            [
                "2017-07-17",
                  array(
                        "start" => "2017-07-16 00:00:00",
                        "end" => "2017-07-18 23:59:59",
                        "renter" => "Renter 1",
                        "vehicle_id" => "1",
                        "price" => "2000",
                        "payment_status" => "1",
                        "status" => "2"
                        )

            ],
            [
                "2017-07-18",
                  array(
                        "start" => "2017-07-16 00:00:00",
                        "end" => "2017-07-18 23:59:59",
                        "renter" => "Renter 1",
                        "vehicle_id" => "1",
                        "price" => "2000",
                        "payment_status" => "1",
                        "status" => "2"
                        )

            ]
        ];
    }






	

}
