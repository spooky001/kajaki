<?php

namespace Tests\Feature\Rent;

use Tests\TestCase;
use App\Models\Rent;
use App\Repositories\RentRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShowTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

 

    public function testShowView(){
    	$mock = \Mockery::mock('App\Repositories\RentRepository'); 
	 
	    $this->app->instance('App\Repositories\RentRepository', $mock);
	 
	    $mock->shouldReceive('findDatesByVehicle')->once();

	    $response = $this->call('GET', 'rents');

	    $response->assertViewHas('dates');

	    $response->assertViewHas('vehicle_id');
    }

	

 
}
