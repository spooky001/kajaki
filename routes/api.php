<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/rent', 'Api\RentController@getAll');

Route::middleware('auth:api')->get('/rent/{id?}', 'Api\RentController@getAllByVehicle');

Route::middleware('auth:api')->get('/rent/accept/{id?}', 'Api\RentController@accept');

Route::middleware('auth:api')->delete('/rent/{id?}', 'Api\RentController@delete');



Route::middleware('auth:api')->post('/vehicle/{id?}', 'Api\VehicleController@update');

Route::middleware('auth:api')->get('/vehicle', 'Api\VehicleController@getAll');

Route::middleware('auth:api')->get('/vehicle/{id?}', 'Api\VehicleController@getAllByUser');

Route::middleware('auth:api')->post('/vehicle/store', 'Api\VehicleController@store');

Route::middleware('auth:api')->delete('/vehicle/{id?}', 'Api\VehicleController@delete');


Route::middleware('auth:api')->post('/user/store', 'Api\UserController@store');

Route::middleware('auth:api')->get('/user', 'Api\UserController@getAll');

Route::middleware('auth:api')->get('/user/{id?}', 'Api\UserController@find');

