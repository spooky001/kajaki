<?php

namespace App\Repositories;

use App\Models\Vehicle;
/**
 * Class BanRepo.
 *
 * @package App\Repository
 */
class VehicleRepository extends BaseRepository
{
    
    public function __construct(Vehicle $model) {
        $this->model = $model;
       
    }

    public function getAll($columns = array('*')) {
            return $this->model->with('user')->get($columns);
    }

    public function getAllByUser($idUser) {
        return $this->model->with('user')->where('user_id','=',$idUser)->get();
    }


    public function create(array $data) {
    	$image_array = $data['image']; 

    	unset($data['image']);
    	
    	$model = $this->model->create($data);

    	$img = str_replace('data:image/png;base64,', '', $image_array);

		$img = str_replace(' ', '+', $image_array);

		$data = base64_decode($image_array);

		file_put_contents(public_path().'/img/vehicles/' . $model->id.".jpg", $data);

		$model->image = $model->id.".jpg";

		$model->save();

        return $model;
    }

    public function update(array $data,$id) {
    	
    	$model = $this->model->find($id);
    	if(isset($data['image']))
    	{
    		$image_array = $data['image']; 

    		unset($data['image']);
    	}
    	
    	$model->update($data);

    	if(isset($image_array))
    	{
	    	$img = str_replace('data:image/png;base64,', '', $image_array);

			$img = str_replace(' ', '+', $image_array);

			$data = base64_decode($image_array);

			file_put_contents(public_path().'/img/vehicles/' . $model->id.".jpg", $data);

			$model->image = $model->id.".jpg";

			$model->save();
		}


		
        return $model;
    }
}
