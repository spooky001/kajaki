<?php

namespace App\Repositories;

use App\Models\User;
/**
 * Class BanRepo.
 *
 * @package App\Repository
 */
class UserRepository extends BaseRepository
{
    
    public function __construct(User $model) {
        $this->model = $model;
       
    }

    public function getAll($columns = array('*')) {
            return $this->model->get($columns);
    }

}
