<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;


class CreateVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
          return [
            'title' => 'required',
            'description' => 'required',
 
            'user_id' => 'required'
        ];
    }


    
    public function filtered()
    {
        $data = $this->all();

    
    }
}
