<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\DefaultController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\VehicleRepository;
use App\Requests\CreateVehicleRequest;

class VehicleController extends DefaultController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
       
    }

     
    public function store(VehicleRepository $vehicle, CreateVehicleRequest $request)
    {
        $success = $vehicle->create($request->all());

        return response()
                ->json($success);
    }

     
    public function getAll(VehicleRepository $vehicle)
    {
        $vehicles = $vehicle->getAll();

        return response()
                ->json($vehicles);
    }


  
    public function getAllByUser(VehicleRepository $vehicle, $id)
    {
        $vehicles = $vehicle->getAllByUser($id);

        return response()
                ->json($vehicles);
    }


    public function delete(VehicleRepository $vehicle, $id)
    {
        
        $status = $vehicle->delete($id);

        if($status)
        {
            return response()
                ->json(['status'=>'success']);
        }
        else{
             return response()
                ->json(['status'=>'fail']);


        }
    }


    public function update(VehicleRepository $vehicle, CreateVehicleRequest $request, $id)
    {
        $success = $vehicle->update($request->all(),$id);

        return response()
                ->json($success);
    }


}
