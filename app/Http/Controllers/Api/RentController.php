<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\DefaultController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\RentRepository;
use App\Requests\CreateRentRequest;

class RentController extends DefaultController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
       
    }

 

      /**
     * Show the application rents.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(RentRepository $rent)
    {
        $rents = $rent->getAll();

        return response()
                ->json($rents);
    }


      /**
     * Show the application rents.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllByVehicle(RentRepository $rent, $id)
    {
        $rents = $rent->getAllByVehicle($id);

        return response()
                ->json($rents);
    }

      /**
     * Show the application rents.
     *
     * @return \Illuminate\Http\Response
     */
    public function accept(RentRepository $rent, $id)
    {
        $rent = $rent->accept($id);

        return response()
                ->json($rent);
    }

    /**
     * Show the application rents.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(RentRepository $rent, $id)
    {
        
        $status = $rent->delete($id);

        if($status)
        {
            return response()
                ->json(['status'=>'success']);
        }
        else{
             return response()
                ->json(['status'=>'fail']);


        }
    }
}
