<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\VehicleRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(VehicleRepository $vehicle)
    {
        $vehicles = $vehicle->getAll();

        return view('home', ['vehicles' => $vehicles]);
    }
}
