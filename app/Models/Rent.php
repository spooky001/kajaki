<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start', 'end', 'renter', 'vehicle_id','price','payment_status','status'
    ];
   


    public function vehicle()
    {
        return $this->belongsTo('App\Models\Vehicle');
    }
}
